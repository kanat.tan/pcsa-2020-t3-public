#include <thread>
#include <unistd.h>
#include "simple_work_queue.hpp"

using namespace std;

struct {
    work_queue work_q;
} shared;

void do_work() {
    for (;;) {
        long w;
        if (shared.work_q.remove_job(&w)) {
            if (w < 0) break; // Terminate with a number < 0
            // NOTE: in fact printf is not thread safe
            printf("%% Thinking for a bit (%lds)\n", w);
            fflush(stdout);
            sleep(w);
            printf("%% Done thinking\n");
            fflush(stdout);
        }
        else {// NO JOB: yield -- let someone else run first
            /* Option 1: continue; */
            /* Option 2: this_thread::yield(); */
            /* Option 3: sleep(0) */
            /* Option 4: usleep(250000); 250ms */
            sleep(0);
        }
        /* Option 5: Go to sleep until it's been notified of changes in the
         * work_queue. Use semaphores or conditional variables
         */
    }
}

int main(int argc, char* argv[]) {
    thread worker(do_work);

    for (;;) {
        long w;
        printf(">>> ");
        fflush(stdout);
        scanf("%ld", &w);

        shared.work_q.add_job(w);
        if (w < 0) { printf("Exiting...\n"); break; }
    }

    worker.join();
}
