#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler

def my_magic_real(n: int) -> int:
   return n**2 + 1


class MyServerHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        # DON'T CODE LIKE ME!
        # Do error checking every time!!!
        content_length = int(self.headers.get('content-length', '0').strip())
        req = self.rfile.read(content_length)

        n = int(req.strip())

        reply = my_magic_real(n)

        self.send_response(200)
        self.end_headers()
        self.wfile.write(bytes(f'{reply}\r\n', 'utf-8'))


web_srv = HTTPServer(('', 8090), MyServerHandler)

try:
    web_srv.serve_forever()
except KeyboardInterrupt:
    print('Exiting...')

web_srv.server_close()
