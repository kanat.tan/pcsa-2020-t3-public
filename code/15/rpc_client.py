#!/usr/bin/env python3

from http.client import HTTPConnection

TARGET = 'localhost:8090'

def my_magic(n: int) -> int:
    message = str(n)
    conn = HTTPConnection(TARGET)
    conn.request('POST', '/', body=message)
    resp = conn.getresponse().read()
    conn.close()
    return int(resp.strip())


print(my_magic(4))
