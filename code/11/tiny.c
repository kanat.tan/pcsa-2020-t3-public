#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include "pcsa_net.h"

/* Rather arbitrary. In real life, be careful with buffer overflow */
#define MAXBUF 1024  

typedef struct sockaddr SA;
void respond_helloworld(int connFd) {
    char buf[MAXBUF];
    char *msg = "<h1>Hello, World</h1> This is a test.";

    sprintf(buf, 
            "HTTP/1.1 200 OK\r\n"
            "Server: Tiny\r\n"
            "Connection: close\r\n"
            "Content-length: %lu\r\n"
            "Content-type: text/html\r\n\r\n", strlen(msg));

    write_all(connFd, buf, strlen(buf));
    write_all(connFd, msg, strlen(msg));
}

void serve_http(int connFd) {
    char buf[MAXBUF];

    if (!read_line(connFd, buf, MAXBUF)) 
        return ;  /* Quit if we can't read the first line */

    printf("LOG: %s\n", buf);
    /* [METHOD] [URI] [HTTPVER] */
    char method[MAXBUF], uri[MAXBUF], httpVer[MAXBUF];
    sscanf(buf, "%s %s %s", method, uri, httpVer);

    if (strcasecmp(method, "GET") == 0 &&
            strcmp(uri, "/") == 0) {
        printf("LOG: Sending hello world\n");
        respond_helloworld(connFd);
    }
    else {
        printf("LOG: Unknown request\n");
    }
        
}

int main(int argc, char* argv[]) {
    int listenFd = open_listenfd(argv[1]);

    for (;;) {
        struct sockaddr_storage clientAddr;
        socklen_t clientLen = sizeof(struct sockaddr_storage);

        int connFd = accept(listenFd, (SA *) &clientAddr, &clientLen);
        if (connFd < 0) { fprintf(stderr, "Failed to accept\n"); continue; }

        char hostBuf[MAXBUF], svcBuf[MAXBUF];
        if (getnameinfo((SA *) &clientAddr, clientLen, 
                        hostBuf, MAXBUF, svcBuf, MAXBUF, 0)==0) 
            printf("Connection from %s:%s\n", hostBuf, svcBuf);
        else
            printf("Connection from ?UNKNOWN?\n");
                
        serve_http(connFd);
        close(connFd);
    }

    return 0;
}
