import http.client

conn = http.client.HTTPConnection(host='cs.muic.mahidol.ac.th', port=80)

conn.set_debuglevel(1)  # this will print the connection-level info (verbose)

headers = {
    'Accept': 'text/plain',
    'Blah': 'Bleh',
    'Connection': 'close',
}

conn.request('GET', '/', headers=headers)  # send the request + headers
resp = conn.getresponse()

print('-------------------------------')
print(resp.status, resp.reason) # Status Code, "Explanation"
print(resp.headers)  # Read all the headers
print(resp.read())   # Print the body
