#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

#ifndef BUFSIZE /* allow for overriding of BUFSIZE from gcc */
#define BUFSIZE 512
#endif

int main(int argc, char* argv[]) {
    
    /* Error handling */
    int inputFd = open(argv[1], O_RDONLY);
    int targetFlags = O_CREAT | O_WRONLY | O_TRUNC;
    int targetFd = open(argv[2], targetFlags, 0644); /* rw-r--r-- */
    char buf[BUFSIZE];

    ssize_t numRead;
    
    while ((numRead = read(inputFd, buf, BUFSIZE)) > 0) {
        /* What about short counts? */
        write(targetFd, buf, numRead);
    }

    close(inputFd);
    close(targetFd);

    return 0;
}
