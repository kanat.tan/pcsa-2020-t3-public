#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>

#define BUFSIZE 1024

int main(int argc, char *argv[]) {
    int numOut = argc - 1;
    int fileDescrs[numOut];

    /* Open the files */
    for (int i=0;i<numOut;i++) 
        fileDescrs[i] = open(argv[i+1], O_CREAT | O_TRUNC | O_WRONLY, 0644);

    ssize_t numRead;
    char buf[BUFSIZE];

    /* Read BUFSIZE bytes */
    while ((numRead = read(STDIN_FILENO, buf, BUFSIZE)) > 0) {
        /* Write the buffer out to every file */
        for (int i=0;i<numOut;i++) 
            write(fileDescrs[i], buf, numRead);
    }
    
    /* Close all the files */
    for (int i=0;i<numOut;i++) 
        close(fileDescrs[i]); 

    return 0;
}
